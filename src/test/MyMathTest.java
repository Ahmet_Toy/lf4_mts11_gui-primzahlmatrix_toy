package test;

import java.util.ArrayList;
import math.myMath;

public class MyMathTest {

  public static void main(String[] args) {

    myMath a = new myMath();
    ArrayList<Integer> zahlen = new ArrayList<>();
    zahlen.add(1);
    zahlen.add(2);
    zahlen.add(5011);
    zahlen.add(9973);
    zahlen.add(10_000);

    ArrayList<Integer> primzahlen = new ArrayList<>();
    ArrayList<Integer> keinePrimzahl = new ArrayList<>();

    for (int element : zahlen) {
      if (a.isPrim(element)) {
        primzahlen.add(element);
      }
      if (!a.isPrim(element)){
        keinePrimzahl.add(element);
      }
    }

    System.out.println("Primzahlen:");
    for (Integer integer : primzahlen) {
      System.out.println(integer);
    }

    System.out.println("\nKeine Primzahlen:");
    for (Integer integer : keinePrimzahl) {
      System.out.println(integer);
    }
  }
}
