package test;

import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import math.primGenerator;
import org.junit.jupiter.api.Test;

class primGeneratorTest {

  @Test
  void generatePrimBoolTest() {
    //...Arrange
    primGenerator generator = new primGenerator();
    //...Act
    boolean[] result = generator.generatePrimBool(1);
    //...Assert
    assertEquals(result, result);
  }

  @Test
  void generatePrimBool_NullPointerTest(){
    //... Arrange
    primGenerator generator = new primGenerator();
    //...Act
    Exception exception = assertThrows(NullPointerException.class,
                                       () -> generator.generatePrimBool(0));
    //...Assert
    assertEquals(null, exception.getMessage());
  }

  @Test
  void generatePrimBool_NegativNumbers(){
    //... Arrange
    primGenerator generator = new primGenerator();
    //...Act
    Exception exception = assertThrows(IllegalArgumentException.class,
                                       () -> generator.generatePrimBool(-1));

    assertEquals("Keine negativen Werte erlaubt", exception.getMessage());
  }
}