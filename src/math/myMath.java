package math;

/*
 * Eine Primzahl ist eine positive Zahl, die nur durch 1 und durch sich selbst ohne Rest teilbar
 * ist. Per Definition ist die 1 keine Primzahl. Primzahlen sind für fast alle Arten von
 * Verschlüsselung wichtig.
 *
 * Um einen Algorithmus zu schreiben, der prüft, ob eine Zahl (n) eine Primzahl ist oder nicht
 * kann folgender Ablauf verwendet werden:
 *
 * Ist n kleiner als 2 ist es keine Primzahl. (Der Befehl ist hierbei return false)
 *
 * Ansonsten prüfe für alle möglichen Teiler von 2 bis n-1 (Übung 5): Wenn n modulo Teiler ==
 * 0 (Übung 2) ist dann wurde ein Teiler gefunden und die Zahl ist keine Primzahl. Wenn die
 * Schleife beendet wird und es wurde kein Teiler gefunden, dann ist die Zahl nur "wahrscheinlich"
 * eine Primzahl (return true; )
 */
public class myMath {

  public myMath(){

  }

  public boolean isPrim(int zahl) {
    if (zahl == 1){ // Per Definition ist die 1 keine Primzahl --> false
      return false;
    }
    if ( zahl == 2){  // Die 2 ist die einzige gerade Primzahl --> true;
      return true;
    }

    int[] basis = {2,3,5,7,11,13,17,19,23,27,29,31,37,41};
    for (int i = 0; i < basis.length; i++){
      if(zahl % basis[i] == 0){
        return false;
      }
    }
    /*
     * Wenn die Zahl im loop durch alle basen nicht ohne Rest teilbar war, dann ist es
     * wahrscheinlich prim --> true
     */
    return true;
  }
}

