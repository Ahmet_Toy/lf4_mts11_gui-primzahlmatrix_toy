package math;

import javax.swing.JOptionPane;

public class primGenerator {

  public primGenerator(){
    //...Konstruktor
  }
  public static int firstNumberOfBlock;
  public static int lastNumberOfBlock;


  /**
   *  Eine Primzahl ist eine positive Zahl, die nur durch 1 und durch sich selbst ohne Rest
   *  teilbar ist. Per Definition ist die 1 keine Primzahl. Primzahlen sind für fast alle Arten von
   *  Verschlüsselung wichtig.
   *
   * Zunächst wird ein bool-Array erzeugt, die 10.000 Werte speichern kann. Im for-Loop wird die
   * gesamte Länge des bool-Arrays durchlaufen und für jede Position i die PrimCheck-Methode aufgerufen.
   * Der Methode wird der Start-Wert übergeben, ab der die nächsten 10.000 Zahlen geprüft werden.
   * @param start erwartet den Startwert ab der für die nächsten 10k Zahlen ein Primzahl-Test ausgeführt wird.
   * @return liefert das bool-Array zurück mit true für alle Primzahlen und false für Nichtprimzahlen.
   */
  public boolean[] generatePrimBool(int start) {

    if(start == 0) {
      throw new NullPointerException();
    }

    if (start < 0) {
      throw new IllegalArgumentException("Keine negativen Werte erlaubt");
    }
    primGenerator.setRangeOfNumbers(start);

    boolean[] primBoolArray = new boolean[10_000]; // Es sollen 10.000 Zahlen geprüft und gespeichert werden
    for (int i = 0; i < primBoolArray.length; i++) {
      primBoolArray[i] = primBoolCheck(i + start);
    }
    return primBoolArray;
  }

  public static boolean primBoolCheck(int number) {

    if (number == 1) { // per Definition keine Primzahl, also liefere false zurück
      return false;
    }
    if (number == 2){ // Die 2 ist die einzige gerade Primzahl.
      return true;
    }

    double sqrRoot = Math.sqrt(number);  // Speichert die Wurzel der als Argument übergebenen Zahl
    for (int i = 2; i <= sqrRoot; i++) {  // es reicht, wenn der Loop bis zur Wurzel der Zahl läuft, die auf eine Primzahl geprüft werden soll
      if (number % i == 0) { // wenn number durch i teilbar ist, dann keine Primzahl --> false
        return false;
      }
    }
    return true;   // wenn der Loop komplett durchlaufen und kein Teiler gefunden wurde, dann muss es eine Primzahl sein --> true
  }

  public static int getStartNumber(){
    return Integer.parseInt((JOptionPane.showInputDialog
                                                 ("Geben Sie einen Startwert an, ab der die " +
                                                      "Primzahlen aus den nächsten 10.000 Zahlen " +
                                                      "ermittelt werden soll:")));
  }

  public static void setRangeOfNumbers(int startNumber){

    primGenerator.firstNumberOfBlock = startNumber; //... speichert den ersten Wert des Blocks
    primGenerator.lastNumberOfBlock = startNumber + 10_000; // ... speichert den letzten Wert des Blocks
  }

  public static void main(String[] args) {

    primNumberGen numberGenerator = new primNumberGen();
    primGenerator boolGenerator = new primGenerator();

    int start = getStartNumber();

    System.out.println("Bool-Array der nächsten 10k Zahlen ab Startwert:");
    boolean[] primzahlen = boolGenerator.generatePrimBool(start);
    for (boolean element: primzahlen){
      System.out.println(element);
    }
    /*System.out.println("Primzahlen der nächsten 10k Zahlen ab Startwert:");
    int[] primNumbers = numberGenerator.generatePrimNumber(start);
    for (int element: primNumbers){
      System.out.println(element);
    }*/
  }
}
