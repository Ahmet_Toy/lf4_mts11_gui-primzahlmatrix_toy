package math;

import java.util.ArrayList;
import java.util.List;

public class primNumberGen {

  List<Integer> notPrimNumb = new ArrayList<>();
  List<Integer> primNumbers = new ArrayList<>();

  public primNumberGen(){
    // ...Konstruktor
  }
  public int[] generatePrimNumber(int start){
    int[] arrayOfPrims = new int[10_000];
    for(int i = 0; i < arrayOfPrims.length; i++){
      primCheck(i+start);
    }
    int[] arrayOfPrimNumbers = new int[primNumbers.size()];
    for (int i = 0; i < primNumbers.size(); i++){
      arrayOfPrimNumbers[i] = primNumbers.get(i);
    }
    notPrimNumb.clear();
    primNumbers.clear();
    return arrayOfPrimNumbers;
  }

  public void primCheck(int number){
    if (number == 1){
      primNumbers.add(number);
    }
    double sqrt = Math.sqrt(number);
    for (int i = 2; i <= sqrt; i++){
      if (number % i == 0){
        notPrimNumb.add(number);
      } else {
        primNumbers.add(number);
      }
    }
  }
}
