package primmatrix;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import math.primGenerator;

public class PrimmatrixGUI extends JFrame {

  final JPanel contentPane;

  /**
   * Launch the application.
   */
  public static void main(String[] args) {
    EventQueue.invokeLater(() -> {
      try {
        PrimmatrixGUI frame = new PrimmatrixGUI();
        frame.setVisible(true);
      } catch (Exception e) {
        e.printStackTrace();
      }
    });
  }

  /**
   * Create the frame.
   */
  public PrimmatrixGUI() {
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setBounds(100, 100, 1400, 900);
    contentPane = new JPanel();
    contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
    setContentPane(contentPane);
    contentPane.setLayout(new BorderLayout(0, 0));

    JPanel panel = new JPanel();
    contentPane.add(panel, BorderLayout.NORTH);

    JPanel gridPanel = new JPanel();
    contentPane.add(gridPanel, BorderLayout.CENTER);
    gridPanel.setLayout(new GridLayout(0, 100, 0, 0));

    primGenerator primGenerator = new primGenerator();
    boolean[] zahlen = primGenerator.generatePrimBool(math.primGenerator.getStartNumber());

    for (int i = 0; i < 10_000; i++){
      JPanel tmp = new JPanel(); //...neues JPanel für eine Zeile erstellen
      tmp.setSize(20,20); //... Jedes Panel ist 5x5 Pixel groß
      tmp.setBorder(new LineBorder(new Color(200, 200, 200)));
      if (zahlen[i] == true){
        tmp.setBackground(Color.BLUE);
      } else {
        tmp.setBackground(Color.WHITE);
      }
      gridPanel.add(tmp); //... Jedes Panel wird zum Grid hinzugefügt.
    }
    setTitle("Primzahl-Matrix der Zahlen von "
                 + math.primGenerator.firstNumberOfBlock + " bis "
                 + math.primGenerator.lastNumberOfBlock);
  }
}
